# Turbo Tasks

This app has been deployed on Fly.io at [https://turbo-tasks.fly.dev](https://turbo-tasks.fly.dev).

## Overview

This is a simple task management app built with Rails 7 and [Hotwire](https://hotwired.dev/). It's an early work in progress, but the basic functionality is there. Known bugs and planned features are documented in the [Issues](https://gitlab.com/biancapower/tasks/-/issues) section.

# User Flow

![User Flow](./docs/turbo-tasks-overview.gif)

## Running Locally

The following are requirements in order to run this app locally:
- Ruby version 3.1.3
- Rails version 7.0.4
- PostgreSQL

Once the above have been installed:
1. clone the repo to your machine
1. `cd` into the application folder
1. `bundle install`
1. `yarn install`
1. `rails db:create`
1. `rails db:migrate`
1. `bin/dev` to start the rails server
