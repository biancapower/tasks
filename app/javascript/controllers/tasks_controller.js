import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="tasks"
export default class extends Controller {
  static targets = [ "checkbox", "input" ]

  toggleCompleted() {
    this.inputTarget.value = this.checkboxTarget.checked
    this.inputTarget.form.requestSubmit()
  }
}
