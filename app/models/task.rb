class Task < ApplicationRecord
  belongs_to :user
  acts_as_list scope: :user
  has_rich_text :content

  validates :description, presence: true
end
