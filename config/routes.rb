Rails.application.routes.draw do
  resources :tasks do
    member do
      patch :move
    end
  end
  devise_for :users
  root to: 'tasks#index'
end
