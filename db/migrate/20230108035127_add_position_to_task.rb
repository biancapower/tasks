class AddPositionToTask < ActiveRecord::Migration[7.0]
  def change
    add_column :tasks, :position, :integer
    Task.order(:created_at).each.with_index(1) do |task, index|
      task.update_column :position, index
    end
  end
end
