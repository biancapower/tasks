class CopyTaskDescriptionToContent < ActiveRecord::Migration[7.0]
  def change
    # copy the description to the content
    Task.all.each do |task|
      task.content = task.description
      task.save
    end
  end
end
