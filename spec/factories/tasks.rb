FactoryBot.define do
  factory :task do
    description { "MyString" }
    completed { false }
  end
end
